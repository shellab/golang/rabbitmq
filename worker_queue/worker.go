package main

import (
	"bytes"
	"log"
	"time"

	"github.com/streadway/amqp"
)

func failOnError(err error, msg string) {
	if err != nil {
		log.Panicf("%s: %s", msg, err)
	}
}

func main() {
	conn, err := amqp.Dial("amqp://root:root@localhost:5672/")
	failOnError(err, "failed to connect to mq")
	defer conn.Close()

	ch, err := conn.Channel()
	failOnError(err, "failed to open a channel")
	defer ch.Close()

	// create a durable queue
	q, err := ch.QueueDeclare(
		"task_queue",
		true,
		false,
		false,
		false,
		nil,
	)
	failOnError(err, "failed to declare a queue")
	// channel quality
	ch.Qos(
		1,     // perfetch count
		0,     // prefetch size
		false, // global
	)
	failOnError(err, "failed to set Qos")

	// create a comsumer
	msgs, err := ch.Consume(
		q.Name,
		"",
		false,
		false,
		false,
		false,
		nil,
	)
	failOnError(err, "failed to register a comsumer")

	forever := make(chan struct{})

	go func() {
		for delivery := range msgs {
			log.Printf("Received a message: %s", delivery.Body)
			dotCount := bytes.Count(delivery.Body, []byte("."))
			time.Sleep(time.Duration(dotCount) * time.Second)
			log.Printf("Done")
			delivery.Ack(false)
			//delivery.Reject(false)
		}
	}()

	log.Printf(" [*] Waiting for messages. To exit press CTRL+C")
	<-forever
}
