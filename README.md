# rabbitmq

some examples to test rabbitmq usage.

## GetStart

> cd docker/ && docker compose up -d

## Run Examples

- terminal 1
    - go run receive.go
- terminal 2
    - go run send.go

## Dashboard

- open http://localhost:15672
- login with user:root, passwd:root
