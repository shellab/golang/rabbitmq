package main

import (
	"log"

	"github.com/streadway/amqp"
)

func failOnError(err error, msg string) {
	if err != nil {
		log.Panicf("%s: %s", msg, err)
	}
}

func main() {
	//conn, err := amqp.Dial("amqp://test:test@rabbitmq.srv.migosrv-dev.com:5672/")
	conn, err := amqp.Dial("amqp://root:root@localhost:5672/")
	failOnError(err, "failed to connect to rabbitMQ")
	defer conn.Close()

	ch, err := conn.Channel()
	failOnError(err, "Failed to open a channel")
	defer ch.Close()

	q, err := ch.QueueDeclare(
		"test_quorum", // queue name
		true,          // durable
		false,         // delete when unused
		false,         // exclusive
		false,         // no-wait
		amqp.Table{
			"x-queue-type": "quorum",
		}, // arguments
	)
	failOnError(err, "Failed to declare a queue")

	msgs, err := ch.Consume(
		q.Name,  // queue name
		"dachi", // comsumer
		true,    // auto Ack
		false,   // exclusive
		false,   // noLocal => not support by RabbitMQ
		false,   // noWait
		nil,     // args
	)
	failOnError(err, "Failed to register a consumer")

	msgs2, err := ch.Consume(
		q.Name,
		"",
		true,
		false,
		false,
		false,
		nil,
	)
	failOnError(err, "Failed to register a consumer")

	//err = ch.Cancel("dachi", false)
	//failOnError(err, "Failed to cancel a consumer")

	forever := make(chan struct{})

	go func() {
		for msg := range msgs {
			log.Printf("from 1 Received a message: %s\n", string(msg.Body))
		}
	}()

	go func() {
		for msg := range msgs2 {
			log.Printf("from 2 Received a message: %s\n", string(msg.Body))
		}
	}()

	log.Printf(" [*] Waiting for messages. To exit press CTRL+C")
	<-forever
}
