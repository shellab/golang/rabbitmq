package main

import (
	"log"
	"time"

	"github.com/streadway/amqp"
)

func failOnError(err error, msg string) {
	if err != nil {
		log.Panicf("%s: %s", msg, err)
	}
}

func main() {
	//conn, err := amqp.Dial("amqp://test:test@rabbitmq.srv.migosrv-dev.com:5672/")
	conn, err := amqp.Dial("amqp://root:root@localhost:5672/")
	failOnError(err, "failed to connect to rabbitMQ")
	defer conn.Close()

	ch, err := conn.Channel()
	failOnError(err, "Failed to open a channel")
	defer ch.Close()

	_, err = ch.QueueDeclare(
		"hello", // queue name
		true,    // durable
		false,   // delete when unused
		false,   // exclusive
		false,    // no-wait
		nil,     // arguments
	)
	failOnError(err, "Failed to declare a queue")

	body := "Hellworld"
	err = ch.Publish("", "hello", false, false, amqp.Publishing{
		Body:        []byte(body),
	})
	failOnError(err, "Failed to publish a message")
	log.Printf("Sent %s\n", body)

	time.Sleep(20 * time.Second)
}
