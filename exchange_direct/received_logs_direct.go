package main

import (
	"log"
	"os"

	"github.com/streadway/amqp"
)

func failOnError(err error, msg string) {
	if err != nil {
		log.Panicf("%s: %s", msg, err)
	}
}

func main() {
	conn, err := amqp.Dial("amqp://root:root@localhost:5672/")
	failOnError(err, "failed to connect mq")
	defer conn.Close()

	ch, err := conn.Channel()
	failOnError(err, "failed to open a chennal")
	defer ch.Close()

	err = ch.ExchangeDeclare(
		"logs_direct",
		"direct",
		false,
		true,
		false,
		false,
		nil,
	)
	failOnError(err, "failed to declare exchange")

	q, err := ch.QueueDeclare(
		"", // random queue name
		false,
		true,  // auto delete
		false, // live cycle with the connection
		false,
		nil,
	)
	failOnError(err, "failed to declare queue")

	if len(os.Args) < 2 {
		log.Printf("Usage: %s [info] [warn] [error]", os.Args[0])
		os.Exit(0)
	}

	// start to bind queue to exchange with the bindkey which from the arguments of the commandline
	for _, v := range os.Args[1:] {
		log.Printf("binding queue %s to exchange %s with routing key %s", q.Name, "logs_direct", v)
		err = ch.QueueBind(
			q.Name,
			v,
			"logs_direct",
			false,
			nil,
		)
		failOnError(err, "failed to bind a queue")
	}

	msgs, err := ch.Consume(
		q.Name,
		"",
		true,
		false,
		false,
		false,
		nil,
	)
	failOnError(err, "Failed to register a consumer")

	forever := make(chan struct{})
	go func() {
		for delivery := range msgs {
			log.Printf("[x] %s", delivery.Body)
		}
	}()

	log.Printf(" [*] Waiting for logs. To exit press CTRL+C")

	<-forever
}
