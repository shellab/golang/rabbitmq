package main

import (
	"fmt"
	"log"

	"github.com/isayme/go-amqp-reconnect/rabbitmq"
)

func init() {
	rabbitmq.Debug = true
}

func failOnError(err error, msg string) {
	if err != nil {
		log.Panicf("%s: %s", msg, err)
	}
}

func main() {
	conn, err := rabbitmq.Dial("amqp://root:root@localhost:5672/")
	failOnError(err, "failed to connect mq")
	defer func() {
		fmt.Printf("++++ main conn close: err: %s\n", conn.Close())
	}()

	ch, err := conn.Channel()
	failOnError(err, "failed to open a chennal")
	defer func() {
		fmt.Printf("---- main channel close: err: %s\n", ch.Close()) // 後 defer 先執行
	}()

	err = ch.ExchangeDeclare(
		"logs",   // exchange name
		"fanout", // exchange type
		true,     // durable
		false,    // auto-deleted
		false,    // internal
		false,    // no-wait
		nil,      // args
	)
	failOnError(err, "Failed to declare an exchange")

	q, err := ch.QueueDeclare(
		"autoconnect", // if name is empty will use random name as queue name
		true,          // durable
		false,         // auto delete
		false,         // use only use in this connection
		false,         // no-wait
		nil,
	)
	failOnError(err, "Failed to declare a queue")

	err = ch.QueueBind(
		q.Name, // which queue name wanna bind.
		"",     // key or match pattern
		"logs", // exchange name
		false,
		nil,
	)
	failOnError(err, "queue failed to bind a exchange")

	// start to consume
	msgs, err := ch.Consume(
		q.Name, // queue name
		"",     // consumer name
		true,   // auto ack
		false,  // exclusive
		false,  // nolocal
		false,  // no-wait
		nil,
	)
	failOnError(err, "Failed to register a consumer")

	forever := make(chan string)

	go func() {
		for delivery := range msgs {
			log.Printf(" [x] %s", delivery.Body)
			if string(delivery.Body) == "exit" {
				log.Println("receive exit signal....")
				break
			}
		}
		fmt.Println("no more message... or exit")
		forever <- "done"
	}()

	log.Printf(" [*] AUTO CONNECT Waiting for logs. To exit press CTRL+C")
	<-forever
}
