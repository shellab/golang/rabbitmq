package main

import (
	"fmt"
	"log"
	"os"
	"strings"

	"github.com/streadway/amqp"
)

func failOnError(err error, msg string) {
	if err != nil {
		log.Printf("%s: %s", msg, err)
	}
}

func bodyFrom(args []string) string {
	var s string
	if (len(args) < 2) || os.Args[1] == "" {
		s = "hello"
	} else {
		s = strings.Join(args[1:], " ")
	}
	return s
}

func main() {
	conn, err := amqp.Dial("amqp://root:root@localhost:5672/")
	failOnError(err, "failed to connect mq")
	defer func () {
		fmt.Printf("conn close: err: %s\n", conn.Close())
	}()

	ch, err := conn.Channel()
	failOnError(err, "failed to open a chennal")
	defer func () {
		fmt.Printf("channel close: err: %s\n", ch.Close())
	}()

	err = ch.ExchangeDeclare(
		"logs",
		"fanout", // type
		true,     // druable
		false,    // auto-deleteed
		false,    // internal
		false,    // no-wait
		nil,      // args
	)
	failOnError(err, "failed to declare exchange")

	body := bodyFrom(os.Args)
	err = ch.Publish(
		"logs", //exchange
		"", // route key
		false, // mandatory
		false, // immediate
		amqp.Publishing{
			ContentType: "text/plain",
			Body: []byte(body),
		},
	)
	failOnError(err, "failed to publish a message to exchange")
	log.Printf(" [x] Sent %s", body)
}
