package main

import (
	"log"
	"fmt"

	"github.com/streadway/amqp"
)

func failOnError(err error, msg string) {
	if err != nil {
		log.Panicf("%s: %s", msg, err)
	}
}

func main() {
	conn, err := amqp.Dial("amqp://root:root@localhost:5672/")
	failOnError(err, "failed to connect mq")
	defer func () {
		fmt.Printf("++++ main conn close: err: %s\n", conn.Close())
	}()

	ch, err := conn.Channel()
	failOnError(err, "failed to open a chennal")
	defer func () {
		fmt.Printf("---- main channel close: err: %s\n", ch.Close()) // 後 defer 先執行
	}()

	go func() {
		for {
			reson, ok := <-ch.NotifyClose(make(chan *amqp.Error))
			if !ok { // ok = false
				fmt.Println("---- get channel close") // channel 被關閉讓 routine 離開
				break
			} else {
				fmt.Printf("---- channel close notify %+v\n", reson) // 發生了 channel 被 close 了
			}
		}
	}()

	go func() {
		for {
			reson, ok := <-conn.NotifyClose(make(chan *amqp.Error)) // 先給個 err 再 close channel
			if !ok {
				fmt.Println("++++ get connection close") // step 2 // 處理 channel close 的狀態，讓 go routine 離開
				break
			} else {
				fmt.Printf("++++ connection close notify %+v\n", reson) // step 1.
			}
		}
	}()

	err = ch.ExchangeDeclare(
		"logs",
		"fanout",
		true,
		false,
		false,
		false,
		nil,
	)
	failOnError(err, "Failed to declare an exchange")

	q, err := ch.QueueDeclare(
		"", // random queue name
		false,
		false,
		true, // use only use in this connection
		false,
		nil,
	)
	failOnError(err, "Failed to declare a queue")

	err = ch.QueueBind(
		q.Name,
		"",
		"logs",
		false,
		nil,
	)
	failOnError(err, "queue failed to bind a exchange")

	// start to consume
	msgs, err := ch.Consume(
		q.Name, // queue name
		"", // consumer name
		true, // auto ack
		false, // exclusive
		false, // nolocal
		false, // no-wait
		nil,
	)
	failOnError(err, "Failed to register a consumer")

	forever := make(chan string)

	go func() {
		for delivery := range msgs {
			log.Printf(" [x] %s", delivery.Body)
		}
		fmt.Println("no more message....")
		forever <- "done"
	}()

	log.Printf(" [*] Waiting for logs. To exit press CTRL+C")
	<-forever
}
