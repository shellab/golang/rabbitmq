package main

import (
	"log"
	"math/rand"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/streadway/amqp"
)

func failOnError(err error, msg string) {
	if err != nil {
		log.Panicf("%s: %s", msg, err)
	}
}

func randomString(l int) string {
	bytes := make([]byte, l)
	for i := 0; i < l; i++ {
		bytes[i] = byte(randInt(65, 90))
	}
	return string(bytes)
}

func randInt(min int, max int) int {
	return min + rand.Intn(max-min)
}

func bodyFrom(args []string) int {
	var s string
	if (len(args) < 2) || os.Args[1] == "" {
		s = "30"
	} else {
		s = strings.Join(args[1:], " ")
	}
	n, err := strconv.Atoi(s)
	failOnError(err, "Failed to convert arg to integer")
	return n
}

func main() {
	// set up rand seed
	rand.Seed(time.Now().UTC().UnixNano())

	n := bodyFrom(os.Args)
	log.Printf(" [x] Requesting fib(%d)", n)

	conn, err := amqp.Dial("amqp://root:root@localhost:5672/")
	failOnError(err, "failed to connect mq")
	defer conn.Close()

	ch, err := conn.Channel()
	failOnError(err, "failed to open a chennal")
	defer ch.Close()

	// create a queue to recevie the reply data of RPC
	q, err := ch.QueueDeclare(
		"",
		false,
		false,
		true,
		false,
		nil,
	)
	failOnError(err, "Failed to declare a queue")

	// consume the reply message the queue
	msgs, err := ch.Consume(
		q.Name,
		"",
		true,
		false,
		false,
		false,
		nil,
	)
	failOnError(err, "Failed to register a consumer")

	corrId := randomString(32)

	// sent to rpc_queue to make a request of Fib RPC
	err = ch.Publish(
		"", // default
		"rpc_queue",
		false,
		false,
		amqp.Publishing{
			ContentType:   "text/plain",
			CorrelationId: corrId, // session id
			ReplyTo:       q.Name, // rpc response callback queue
			Body:          []byte(strconv.Itoa(n)),
		},
	)
	failOnError(err, "Failed to publish a message")

	var res int
	for d := range msgs {
		if corrId == d.CorrelationId { // match the unique number
			res, err = strconv.Atoi(string(d.Body))
			failOnError(err, "Failed to convert body to integer")
			break
		}
	}
	log.Printf(" [.] Got %d", res)
}
