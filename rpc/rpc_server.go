package main

import (
	"log"
	"strconv"

	"github.com/streadway/amqp"
)

func failOnError(err error, msg string) {
	if err != nil {
		log.Panicf("%s: %s", msg, err)
	}
}

func fib(n int) int {
	if n == 0 {
		return 0
	} else if n == 1 {
		return 1
	} else {
		return fib(n-1) + fib(n-2)
	}
}

func main() {
	conn, err := amqp.Dial("amqp://root:root@localhost:5672/")
	failOnError(err, "failed to connect mq")
	defer conn.Close()

	ch, err := conn.Channel()
	failOnError(err, "failed to open a chennal")
	defer ch.Close()

	// provide a request queue for RPC service
	q, err := ch.QueueDeclare(
		"rpc_queue",
		true,
		false,
		false,
		false,
		nil,
	)
	failOnError(err, "Failed to declare a queue")

	err = ch.Qos(
		1,     // prefetch count
		0,     // prefetch size
		false, // global
	)
	failOnError(err, "Failed to set QoS")

	// comsume RPC request from rpc_queue
	msgs, err := ch.Consume(
		q.Name, // queue
		"",     // consumer
		false,  // auto-ack
		false,  // exclusive
		false,  // no-local
		false,  // no-wait
		nil,    // args
	)
	failOnError(err, "Failed to register a consumer")

	forever := make(chan struct{})

	go func() {
		for d := range msgs {
			n, err := strconv.Atoi(string(d.Body))
			failOnError(err, "Failed to convert body to integer")

			log.Printf(" [.] fib(%d)", n)
			response := fib(n) // do process of fib function
			err = ch.Publish(
				"",        // exchange
				d.ReplyTo, // reply to queue which client defined on-the-fly.
				false,
				false,
				amqp.Publishing{
					ContentType:   "text/plain",
					CorrelationId: d.CorrelationId, // use same session id to communicate with client
					Body:          []byte(strconv.Itoa(response)),
				},
			)
			failOnError(err, "Failed to publish a message")
			d.Ack(false) // need to do, because auto-ack is false
		}
	}()

	log.Printf(" [*] Awaiting RPC requests")
	<-forever
}
